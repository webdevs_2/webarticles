<?php

namespace App\DataFixtures;

use App\Entity\Level;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LevelFixtures extends Fixture
{
    private array $levels = ['Débutant','Avancé','Expert'];
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        foreach ($this->levels as $data) {
            $level = new Level();
            $level->setLevel($data);
            $manager->persist($level);
        }

        $manager->flush();
    }
}
